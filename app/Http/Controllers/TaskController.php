<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Task; 
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Gate;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;


class TaskController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //get all tasks
       $tasks = Task::all();
       return view('tasks.index', ['tasks' => $tasks]);
    }
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view ('tasks.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */

    public function store(Request $request)
    {
        $tasks= new Task();
        $id = Auth::id();
        $tasks->title = $request->title;
        $tasks->status=0;
        $tasks->user_id=$id;
        $tasks->save();
        return redirect('tasks');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $task = Task::find($id);
        return view('tasks.edit', compact('task'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $task = Task::find($id);
        $task->update($request->all());
        return redirect('tasks');  
    }

  
    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        if (Gate::denies('admin')) {
          abort(403,"Sorry you are not allowed to delete..");
       }
        $task = Task::find($id);
        $task->delete();
        return redirect('tasks');    
    }

         public function done($id)
        {
            //only if this todo belongs to user 
            if (Gate::denies('admin')) {
                abort(403,"You are not allowed to mark tasks as dome..");
             }          
            $task = Task::findOrFail($id);            
            $task->status = 1; 
            $task->save();
            return redirect('tasks');    
        }  
        public function indexFiltered()
        {
            $id = Auth::id(); //the current user 
            $tasks = Task::where('user_id', $id)->get();
            $filtered = 1; //this is to mark the view to show link to all tasks 
            return view('tasks.index', compact('tasks','filtered'));
        }

      /*  public function ShowOnliDone($id)
        {
           //the current user 
            $tasks = Task::where('status', 1)->get();
           // $tasks = Task::$task->status = 1;
            return view('tasks.index', ['tasks' => $tasks]);
        }
        

        public function change_status($id,$status)
        {
           return view('tasks.index', ['id'=>$id, 'status'=>$status]);
        }    

      
        */    

}
