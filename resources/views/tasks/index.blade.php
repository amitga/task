@yield('content')

@extends('layouts.app')
@section('content')

<h1> This is the tasks list</h1>
@isset($filtered)
     <a href = {{route('tasks.index')}}>All Tasks</a>
    @else
     <a href = {{route('myfilter')}}>My tasks</a>
@endisset

<table>
<tr>
    <th>Task</th>
    <th>Status</th>
    <th>Edit</th>
    @cannot('user') <th>Delete</th> @endcannot
    @can('admin') <th>Mark as done</th> @endcan
    
</tr>
    @foreach($tasks as $task)
<tr>
    <td> {{$task->title}}</td>
    <td> @if ($task->status)
           <input type = 'checkbox' id ="{{$task->id}}" disabled='disable' checked>
       @else
           <input type = 'checkbox' id ="{{$task->id}}">
       @endif </td>
    <td> <a href = "{{route('tasks.edit',$task->id)}}"> Edit</a> </td>
   
    @cannot('user')<td> 

    <form method = 'post' action = "{{action('TaskController@destroy', $task->id)}}" > 

    @csrf
    @method('DELETE')

    <div class = "form-group">
        <input type = "submit" class = "form-control" name = "submit" value = "Delete">
    </div>
    </td> @endcannot
    </form>
    @can('admin') <td> @if ($task->status == 0)
    <a href="{{route('done', $task->id)}}">Mark As done</a></td>
    @else
    Done!
    @endif </td> @endcan
</tr>
    @endforeach
    </table>

    <a href = "{{route('tasks.create')}}"> Create a new task</a>

    

    @endsection

    <style>
table, th, td {
  border: 1px solid black;
}
    </style>




  
