<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('tasks/filter/', 'TaskController@indexFiltered')->name('myfilter');

//Route::get('tasks/ShowOnliDone/{id}', 'TaskController@ShowOnliDone')->name('ShowOnliDone');
//<!--<a href = "{{route('ShowOnliDone', $tasks->id)}}"> Show onli done tasks</a>-->

Route::get('/home', 'HomeController@index')->name('home');

Route::resource('tasks', 'TaskController')->middleware('auth');

//Route::get('tasks/delete/{id}', 'TaskController@destroy')->name('delete');

Route::get('tasks/done/{id}', 'TaskController@done')->name('done');






